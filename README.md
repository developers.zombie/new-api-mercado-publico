# APP PARA CONSULTA A LA API API MERCADO PÚBLICO

## requerimiento

- Nodejs > = v14.16.0

## INSTALACIÓN Y USO DE LA APP

- CLONAR EL REPOSITORIO

```
 git clone git@gitlab.com:developers.zombie/new-api-mercado-publico.git

```

- CLONADO EL REPOSITORIO INGRESAMOS A LA CARPETA GENERADA DE LA APP


```
 cd new-api-mercado-publico

```

- INSTALAMOS LAS DEPENDENCIAS DE LA APP

```
 npm install

```

- COPIAMOS EL ARCHIVIO .env-example Al ARCHIVO .env 

```
 cp .env-example .env

```

- VARIABLES POR DEFECTO EN EL ARCHIVO .env

```
LIMIT_DEFAUT=100

FECHA_DEFAUT=05092020

```

LIMIT_DEFAUT valor limite base para buscar los registros

FECHA_DEFAUT fecha base para realizar la busqueda

- NOTA FORMATO DE LA FECHA AL SER INGRESADA  DIA MES AÑO  EJEMPLO 10102020

- INICIAL LA APP

```
 npm start 

```

- EL ARCHIVIO DE EXCEL GENERADO SE GUARDA EN LA CARPETA  /file_xlsx QUE ESTA DENTRO DE LA CARPETA DE LA APP
