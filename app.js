require('dotenv').config();
const inquirer = require('inquirer');
const prompt = require('prompt-sync')();
const services = require('./src/sevice');


const tickets = {
  key1: process.env.TICKET_1,
  key2: process.env.TICKET_2,
  key3: process.env.TICKET_3,
  key4: process.env.TICKET_4
};


inquirer.prompt([{
  type: 'list',
  name: 'opt',
  message: 'what is your option?',
  choices: [
      {
          value:1,
          name:'Search API MercadoPublico '
      }
  ],
}]).then(
async (res) => {
  
  switch (res.opt) {
    case 1:

      let setFecha =  prompt("Enter date with this format (d m a) => (05092020): ");
      
      let numR =  prompt("Enter the record limit: ");

      let fecha_base = setFecha === '' ? process.env.FECHA_DEFAUT: setFecha;

      let limit = numR === '' ? process.env.LIMIT_DEFAUT: numR;

      services._genesis(process.env.URL, process.env.TICKET_BASE, fecha_base).then( data => {

        services._getDataForCode(data, process.env.URL, tickets , limit).then(dta => {
      
          services._setExcel({
            data: dta,
            fecha: fecha_base,
            file: process.env.RUTA_EXCEL + fecha_base + ".xlsx"
          });

        });
      
      });

     
    break;
    default:
      process.exit(0);
    break;
 }
}
);
