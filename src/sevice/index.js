require('dotenv').config();
const fetch = require("node-fetch");
const fs = require('fs');
const excel = require('excel4node');
const download = require('download');



async function _genesis(url_base, ticket_base, fecha) {
  
  console.log("CONNECTED WITH API MERCADO-PUBLICO...");

  try {
    //sacando los codigos de las ordenes
    let _data = [];

    let url_ini = `${url_base}?fecha=${fecha}&ticket=${ticket_base}`;

    let response = await fetch(url_ini);

    let data = await response.json();

    _data = data.Listado.map(dtodo => {
      return {
        codigo: dtodo.Codigo
      };
    });

    return _data;

  } catch (error) {
    //console.log(error);
  }

}



function _getDataForCode(dta, url_base, tickets , limit) {

  console.log("PROCESSING " + limit + " RECORDS...")

  return new Promise((resolve, reject) => {
    let dataExel = [];

    let i = 0;
    let key = 1;
    let ticket = null;

    let inter = setInterval(function () {
      if (i > limit) {
        inter = clearInterval(inter);
        resolve(dataExel);
      } else {

        if (key === 1) {
          ticket = tickets.key1;
        } else if (key === 2) {
          ticket = tickets.key2;
        } else if (key === 3) {
          ticket = tickets.key3;
        } else {
          ticket = tickets.key4;
        }

        let url_data = `${url_base}?codigo=${dta[i].codigo}&ticket=${ticket}`;

        fetch(url_data).then(function (response) {
            if (response.ok) {
              return response.json();
            }
        }).then(function (data) {
            if (data.Listado === undefined) {
              console.log('NO RESULTS');
            }

            dataExel.push({
              codigo: data.Listado[0].Codigo,
              nombreComprador: data.Listado[0].Comprador.NombreOrganismo,
              rutComprador: data.Listado[0].Comprador.RutUnidad,
              telComprador: data.Listado[0].Comprador.FonoContacto,
              emailComprador: data.Listado[0].Comprador.MailContacto,
              nombreProveedor: data.Listado[0].Proveedor.Nombre,
              rutProveedorSucursal: data.Listado[0].Proveedor.RutSucursal,
              telefonoProveedor: data.Listado[0].Proveedor.FonoContacto,
              emailProveedor: data.Listado[0].Proveedor.MailContacto,
              nombreContacto: data.Listado[0].Proveedor.NombreContacto,
              telefonoContacto: data.Listado[0].Proveedor.FonoContacto,
              total: data.Listado[0].Total,
              totalNeto: data.Listado[0].TotalNeto,
              tipoMoneda: data.Listado[0].TipoMoneda,
              regionProveedor: data.Listado[0].Proveedor.Region,
              fechaCreacion:data.Listado[0].Fechas.FechaCreacion
              
            });

            //CAMBIO DE KEY
            if (key === 4) {
              key = 1;
            } else {
              key++;
            }
            i++;
          }).catch(err => {

          });
      }
    }, 2000);

  });
}



function formt_fecha(fecha) {
  let arrayFecha = fecha.split("");
  let fechaFormat =
    arrayFecha[4] +
    arrayFecha[5] +
    arrayFecha[6] +
    arrayFecha[7] +
    arrayFecha[2] +
    arrayFecha[3] +
    arrayFecha[0] +
    arrayFecha[1];
  return fechaFormat;
}



//GENERATE FILE EXCEL
function _setExcel(data) {

  let workbook = new excel.Workbook();

  let worksheet = workbook.addWorksheet(data.fecha);

  let style = workbook.createStyle({
    font: {
      color: '#FF0800',
      size: 12
    },
    numberFormat: '$#,##0.00; ($#,##0.00); -'
  });

  worksheet.cell(1, 1).string("CODIGO").style(style);
  worksheet.cell(1, 2).string("NOMBRE COMPRADOR").style(style);
  worksheet.cell(1, 3).string("RUT COMPRADOR").style(style);
  worksheet.cell(1, 4).string("TELF  COMPRADOR").style(style);
  worksheet.cell(1, 5).string("EMAIL COMPRADOR").style(style);
  worksheet.cell(1, 6).string("NOMBRE PROVEEDOR").style(style);
  worksheet.cell(1, 7).string("RUT PROVEEDOR").style(style);
  worksheet.cell(1, 8).string("TELF PROVEEDOR").style(style);
  worksheet.cell(1, 9).string("EMAIL PROVEEDOR").style(style);
  worksheet.cell(1, 10).string("NOMBRE CONTACTO PROVEEDOR").style(style);
  worksheet.cell(1, 11).string("TELF PROVEEDOR").style(style);
  worksheet.cell(1, 12).string("TOTAL").style(style);
  worksheet.cell(1, 13).string("TOTAL NETO").style(style);
  worksheet.cell(1, 14).string("TIPO DE MONEDA").style(style);
  worksheet.cell(1, 15).string("REGION PROVEEDOR").style(style);
  worksheet.cell(1, 16).string("FECHA DE CREACIÓN").style(style);

  data.data.forEach((elem, index) => {
    index += 2;
    worksheet.cell(index, 1).string(elem.codigo.toString());
    worksheet.cell(index, 2).string(elem.nombreComprador.toString());
    worksheet.cell(index, 3).string(elem.rutComprador.toString());
    worksheet.cell(index, 4).string(elem.telComprador.toString());
    worksheet.cell(index, 5).string(elem.emailComprador.toString());
    worksheet.cell(index, 6).string(elem.nombreProveedor.toString());
    worksheet.cell(index, 7).string(elem.rutProveedorSucursal.toString());
    worksheet.cell(index, 8).string(elem.telefonoProveedor.toString());
    worksheet.cell(index, 9).string(elem.emailProveedor.toString());
    worksheet.cell(index, 10).string(elem.nombreContacto.toString());
    worksheet.cell(index, 11).string(elem.telefonoContacto.toString());
    worksheet.cell(index, 12).string(elem.total.toString());
    worksheet.cell(index, 13).string(elem.totalNeto.toString());
    worksheet.cell(index, 14).string(elem.tipoMoneda.toString());
    worksheet.cell(index, 15).string(elem.regionProveedor.toString());
    worksheet.cell(index, 16).string(elem.fechaCreacion.toString());
  });
  
  console.log("EXCEL CREATED")
  return workbook.write(data.file);
}

//BETA
async function _download(file,filePath) {
  await download(file,filePath);
  console.log('Download Completed');
}


module.exports = {
  _getDataForCode,
  _setExcel,
  _genesis,
  _download
}